Evolução Cotação

![Evolução Cotação](dados/grafico.png).

Projeto para importação de dados históricos da cotação de produtos da COTRIEL.

Referências:
- https://pbpython.com/pandas-html-table.html
- https://pandas.pydata.org/pandas-docs/stable/index.html
- https://matplotlib.org/



Setup e Run:
```
python3 -m venv .python-venv
source .python-venv/bin/activate
pip install -r requirements.txt
python importa_cotacao.py
```

Run on docker
```
docker run -it -v $(pwd):$(grep WORKDIR Dockerfile | tail -n 1 | awk '{print $2}') registry.gitlab.com/josuebragagnolo/importacao-cotriel/importacao-cotriel:latest
```
