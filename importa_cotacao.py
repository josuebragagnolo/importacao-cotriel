import os
from datetime import datetime
import matplotlib.pyplot as plt
import pandas as pd
from sqlalchemy import *
from dotenv import load_dotenv
import plotly.express as px

load_dotenv()

db_string = os.getenv('PGSQL_URL')
engine = create_engine(db_string)
conn = engine.connect()

teste = False
outfile = 'dados/cotacao.csv'
df_modificado = False

def toPostgresql(df):
    df_schema = {
        "data": DateTime(),
        "soja": Numeric,
        "milho": Numeric,
        "trigo": Numeric,
        "arroz": Numeric
    }    
    #print(f"str: " + db_string)
    print("toPostgresql...")
    df.to_sql("cotacao", con=engine, index_label='id', index=True, if_exists='replace',dtype=df_schema)

def importaMes(ano, mes):
    global df_modificado
    df_modificado = True
    urlbase = "https://www.cotriel.com.br/Cotacoes"
    print(f"importando " + str(ano) + "-" + str(mes))
    dfmes = pd.read_html(urlbase+"?mes="+str(mes)+"&ano=" + str(ano), match="Soja", thousands='.', decimal=',')[0]
    dfmes['Dia'] = dfmes['Dia'].apply(str)
    dfmes['Dia'] = pd.to_datetime(str(ano) + "-" + str(mes) + "-" + dfmes['Dia'])

    #print(f'Total tables: {len(table_MN)}')
    dfmes = dfmes.replace({"R\$": ''}, regex=True).replace({"\.": ''}, regex=True).replace({"\,": '.'}, regex=True).replace({"\s+": ''}, regex=True)
    dfmes.columns = ['data', 'periodo', 'soja', 'milho', 'trigo', 'arroz']
    dfmes.drop('periodo',inplace=True, axis=1)
    dfmes['soja'] = dfmes['soja'].astype('str').str.extractall('(\d+)(\.)').unstack().fillna('').sum(axis=1).apply(pd.to_numeric)
    dfmes['milho'] = dfmes['milho'].astype('str').str.extractall('(\d+)(\.)').unstack().fillna('').sum(axis=1).apply(pd.to_numeric)
    dfmes['trigo'] = dfmes['trigo'].astype('str').str.extractall('(\d+)(\.)').unstack().fillna('').sum(axis=1).apply(pd.to_numeric)
    dfmes['arroz'] = dfmes['arroz'].astype('str').str.extractall('(\d+)(\.)').unstack().fillna('').sum(axis=1).apply(pd.to_numeric)
    dfmes = dfmes.groupby(['data']).mean().reset_index()

    # print(dfmes)
    return dfmes

def main(): ## (primeira execucao) se nao existir dados locais, nesse caso importa de hoje até que haja mais dados.
    if not os.path.exists(outfile):
        ano = int(datetime.today().strftime('%Y'))
        mes = int(datetime.today().strftime('%m'))
        tbl_size = 1
        df = pd.DataFrame()
        table_tmp = pd.DataFrame()

        while tbl_size > 0:
            table_tmp = importaMes(ano, mes)

            tbl_size = table_tmp.size
            if tbl_size > 0:
                df = pd.concat([df, table_tmp], ignore_index=True)

            if mes == 1:
                mes = 12
                ano -= 1
            else:
                mes -= 1

            if teste:
                mes = 12
                ano = 2008
                teste = False

    else:
        df = pd.read_sql_table('cotacao', conn,index_col='id')
        conn.close()
        print(df)
        # output = conn.execute(text("SELECT MAX(data) FROM cotacao LIMIT 1"))
        # results = output.fetchone()
        # print(results[0])
        # ultimo_dado = results[0]
        ultimo_dado = df.iloc[-1]['data']
        print("ultimo_dado: " + ultimo_dado.strftime("%Y-%m-%d %H:%M:%S"))
        if ultimo_dado.strftime('%Y-%m-%d') != datetime.today().strftime('%Y-%m-%d'):
            hoje_ano = int(datetime.today().strftime('%Y'))
            hoje_mes = int(datetime.today().strftime('%m'))

            ano = int(ultimo_dado.strftime('%Y'))
            mes = int(ultimo_dado.strftime('%m'))

            while True:
                table_tmp = importaMes(ano, mes)
                df = pd.concat([df, table_tmp]).drop_duplicates().reset_index(drop=True)
                if (ano == hoje_ano and mes == hoje_mes):
                    break
                elif mes == 12:
                    ano += 1
                    mes = 1
                else:
                    mes += 1
        else:
            print("sem dados para importar")

    if df_modificado:
        toPostgresql(df)
        df.sort_values(by=['data'], inplace=True)
        # df.to_csv(outfile, index=False)
        # plt.style.use('seaborn-whitegrid')
        # df.plot.line(x='data', y=['soja', 'trigo'])
        # # plt.show()
        # plt.savefig('dados/grafico.png', bbox_inches='tight')

        ############
        fig = px.line(df, x='data', y=['soja', 'trigo', 'milho'], title='Cotação Cotriel')
        
        # Customize the plot appearance
        fig.update_traces(mode='lines+markers', marker=dict(size=2, symbol='circle'), line=dict(width=2))
        fig.update_layout(title_font_size=24, title_font_family='Arial',
                        xaxis=dict(title='Date', tickfont=dict(size=12),
                                    tickformat='%Y-%m-%d'),  # Format the date
                        yaxis=dict(title='Cotação', tickfont=dict(size=12),
                                    tickformat='.2f'),  # Display y-axis with 2 decimal places
                        hovermode='x unified'
                                    )
        fig.update_yaxes(title_font=dict(size=12))
        # # Customize the tooltip format
        fig.update_traces(hovertemplate='R$ %{y:.2f}')
        ##############
        if not os.path.isdir("public"):
            os.makedirs("public")
        fig.write_html("public/index.html")


if __name__ == '__main__':
    main()
