FROM python:3.8-slim AS importacao-cotriel
WORKDIR /usr/src/app
ENV PYTHONPATH=.

# ENV VIRTUAL_ENV=./venv
# RUN python3 -m venv $VIRTUAL_ENV
# ENV PATH="$VIRTUAL_ENV/bin:$PATH"

COPY requirements.txt ./

# RUN apt-get update -y && \
#     apt-get install git -y && \
#     apt-get autoclean -y && \
#     apt-get autoremove -y && \
#     rm -rfv /var/cache/apt/ || true && \
#     python -m pip install --upgrade pip && \
#     pip install --no-cache-dir -r requirements.txt

RUN python -m pip install --upgrade pip && \
    pip install --no-cache-dir -r requirements.txt

#CMD [ "python", "./importa_cotacao.py" ]


# FROM builder AS importacao-cotriel
# WORKDIR /usr/src/app
# ADD . .
# CMD [ "python", "./importa_cotacao.py" ]


# FROM gcr.io/distroless/python3:latest AS importacao-cotriel
# WORKDIR /usr/src/app
# COPY --from=builder /usr/src/app .

# CMD [ "./importa_cotacao.py" ]